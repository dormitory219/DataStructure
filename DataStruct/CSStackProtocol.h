//
//  CSStackProtocol.h
//  DataStruct
//
//  Created by 韩小猫爱吃鱼 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#ifndef CSStackProtocol_h
#define CSStackProtocol_h

@protocol CSStackProtocol <NSObject>

- (NSInteger)count;

- (void)push:(id)object;

- (id)pop;

@end

#endif /* CSStackProtocol_h */
