//
//  CSSetProtocol.h
//  DataStruct
//
//  Created by 韩小猫爱吃鱼 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#ifndef CSSetProtocol_h
#define CSSetProtocol_h

@protocol CSSetProtocol <NSObject>

- (NSInteger)count;

- (void)addObject:(id)object;

- (BOOL)removeObject:(id)object;

@end


#endif /* CSSetProtocol_h */
