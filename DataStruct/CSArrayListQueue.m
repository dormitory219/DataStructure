//
//  CSArrayListQueue.m
//  DataStruct
//
//  Created by 余强 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import "CSArrayListQueue.h"
#import "CSArrayList.h"

@interface CSArrayListQueue()
{
    //使用动态数组作为底层结构
    CSArrayList *_arrayList;
}
@end

@implementation CSArrayListQueue

- (instancetype)init
{
    self = [super init];
    if (self) {
        _arrayList = [CSArrayList array];
    }
    return self;
}

+ (instancetype)arrayQueue
{
    return [[self alloc] init];
}

- (NSInteger)count
{
    return [_arrayList count];
}

- (void)queue:(id)object
{
    [_arrayList addObject:object];
}

- (id)dequeue
{
    id object = [_arrayList firstObject];
    [_arrayList removeObject:object];
    return object;
}

- (NSString *)description
{
    NSMutableString *res = [NSMutableString string];
    [res appendFormat:@"ArrayListQueue: %p \n",self];
    [res appendString:@"top"];
    [res appendString:@" [ "];
    for (int i = 0; i<_arrayList.count; i++)
    {
        id object = [_arrayList objectAtIndex:i];
        [res appendFormat:@"%@",object];
        if (i != _arrayList.count - 1)
        {
            [res appendString:@" , "];
        }
    }
    [res appendString:@" ]"];
    return res;
}

@end
