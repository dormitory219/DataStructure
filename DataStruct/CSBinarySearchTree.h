//
//  CSBinarySearchTree.h
//  DataStruct
//
//  Created by 余强 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSBSTNode: NSObject

@property (nonatomic,strong) CSBSTNode *left;
@property (nonatomic,strong) CSBSTNode *right;
@property (nonatomic,strong) id data;

@end

//BST实现
@interface CSBinarySearchTree : NSObject

+ (instancetype)bst;

- (void)addObject:(id)object;

@end
