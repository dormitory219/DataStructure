//
//  CSLinkedListStack.h
//  DataStruct
//
//  Created by 韩小猫爱吃鱼 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import "CSLinkedList.h"
#import "CSStackProtocol.h"

@interface CSLinkedListStack : NSObject<CSStackProtocol>

+ (instancetype)linkStack;

@end
