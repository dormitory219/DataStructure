//
//  CSLinkedListQueue.m
//  DataStruct
//
//  Created by 韩小猫爱吃鱼 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import "CSLinkedListQueue.h"
#import "CSLinkedList.h"

@interface CSLinkedListQueue()
{
    //使用链表作为底层结构
    CSLinkedList *_linkedList;
}
@end

@implementation CSLinkedListQueue

- (instancetype)init
{
    self = [super init];
    if (self) {
        _linkedList = [CSLinkedList linklist];
    }
    return self;
}

+ (instancetype)linkQueue
{
    return [[self alloc] init];
}

- (NSInteger)count
{
    return [_linkedList count];
}

- (void)queue:(id)object
{
    [_linkedList addObject:object];
}

- (id)dequeue
{
    id object = [_linkedList firstObject];
    [_linkedList removeObject:object];
    return object;
}

- (NSString *)description
{
    NSMutableString *res = [NSMutableString string];
    [res appendFormat:@"LinkedListQueue: %p \n",self];
    [res appendString:@"top"];
    [res appendString:@" [ "];
    for (int i = 0; i<_linkedList.count; i++)
    {
        id object = [_linkedList objectAtIndex:i];
        [res appendFormat:@"%@",object];
        if (i != _linkedList.count - 1)
        {
            [res appendString:@" , "];
        }
    }
    [res appendString:@" ]"];
    return res;
}

@end
