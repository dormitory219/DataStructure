//
//  CSLinkedListSet.m
//  DataStruct
//
//  Created by 韩小猫爱吃鱼 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import "CSLinkedListSet.h"

@implementation CSLinkedListSet

+ (instancetype)set
{
    return [super linklist];
}

- (NSInteger)count
{
    return [super count];
}

- (void)addObject:(id)object
{
    if ([self containsObject:object])
    {
        return;
    }
    [self addObject:object];
}

- (BOOL)removeObject:(id)object
{
   return [self removeObject:object];
}

@end
