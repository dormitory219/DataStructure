//
//  CSLinkedList.h
//  DataStruct
//
//  Created by 余强 on 2018/8/20.
//  Copyright © 2018年 余强. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CSLinkedListProtocol <NSObject>

- (int)count;

- (BOOL)isEmpty;

- (id)objectAtIndex:(NSUInteger)index;

- (void)addObject:(id)object;

- (void)insertObject:(id)object atIndex:(int)index;

- (BOOL)removeObject:(id)object;

- (BOOL)containsObject:(id)object;

- (id)firstObject;

- (id)lastObject;

@end

/*
 typedef void* AnyObject;
 
 typedef struct node {
 AnyObject data;
 struct node *next;
 } Node;
 
 - (instancetype)init
 {
     self = [super init];
     if (self) {
     Node * dummyHead = (Node*)malloc(sizeof(Node));
     dummyHead->data = nil;
     dummyHead->next = nil;
     self.dummyHead = dummyHead;
     self.size = 0;
     }
     return self;
 }
 
 */

@interface CSNode : NSObject

@property (nonatomic,strong) CSNode *next;
@property (nonatomic,strong) id data;

+ (instancetype)node;

+ (instancetype)nodeWithData:(id)value;

+ (instancetype)nodeWithData:(id)value nextNode:(CSNode *)nextNode;

@end

@interface CSLinkedList : NSObject<CSLinkedListProtocol>

+ (instancetype)linklist;

@end
