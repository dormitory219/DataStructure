//
//  CSLinkedListStack.m
//  DataStruct
//
//  Created by 韩小猫爱吃鱼 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import "CSLinkedListStack.h"
#import "CSLinkedList.h"

@interface CSLinkedListStack()
{
    //使用链表作为底层结构
    CSLinkedList *_linkedList;
}
@end

@implementation CSLinkedListStack

- (instancetype)init
{
    self = [super init];
    if (self) {
        _linkedList = [CSLinkedList linklist];
    }
    return self;
}

+ (instancetype)linkStack
{
    return [[self alloc] init];
}

- (NSInteger)count
{
    return _linkedList.count;
}

- (void)push:(id)object
{
    [_linkedList addObject:object];
}

- (id)pop
{
    id object = [_linkedList lastObject];
    [_linkedList removeObject:object];
    return object;
}

- (NSString *)description
{
    NSMutableString *res = [NSMutableString string];
    [res appendFormat:@"LinkedListStack: %p \n",self];
    [res appendString:@" [ "];
    for (int i = 0; i<_linkedList.count; i++)
    {
        id object = [_linkedList objectAtIndex:i];
        [res appendFormat:@"%@",object];
        if (i != _linkedList.count - 1)
        {
            [res appendString:@" , "];
        }
    }
    [res appendString:@" ] top "];
    return res;
}

@end
