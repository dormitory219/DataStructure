//
//  CSMapProtocol.h
//  DataStruct
//
//  Created by 韩小猫爱吃鱼 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#ifndef CSMapProtocol_h
#define CSMapProtocol_h

@protocol CSMapProtocol <NSObject>

- (NSInteger)count;

- (void)setObject:(id)object forKey:(NSString *)key;

- (id)objectForKey:(NSString *)key;

@end

#endif /* CSMapProtocol_h */
