//
//  CSArray.m
//  DataStruct
//
//  Created by 余强 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import "CSArrayList.h"

static NSInteger const defaultCapacity = 10;
typedef id _Nullable (*RFUNC)(id _Nonnull, SEL _Nonnull,...);
typedef void * AnyObject;

@interface CSArrayList()

@property (nonatomic,assign) NSInteger size;

@property (nonatomic,assign) NSInteger capacity;

 //使用静态C数组作为底层结构
@property (nonatomic,assign) AnyObject *array;

@end


@implementation CSArrayList

- (instancetype)init
{
    self = [super init];
    if (self) {
        _size = 0;
        _capacity = defaultCapacity;
        _array = (AnyObject*)calloc(_capacity, sizeof(AnyObject));
    }
    return self;
}

+ (instancetype)array
{
    return [[self alloc] initWithCapacity:defaultCapacity];
}

+ (instancetype)arrayWithCapacity:(NSUInteger)numItems
{
    return [[self alloc] initWithCapacity:numItems];
}

- (instancetype)initWithCapacity:(NSUInteger)numItems
{
    _capacity = numItems;
    _array = (AnyObject*)calloc(_capacity,sizeof(AnyObject));
    _size = 0;
    return self;
}

#pragma mark - 增加操作
- (void)addObject:(id)object
{
    [self insertObject:object atIndex:_size];
}

- (void)insertObject:(id)object atIndex:(NSUInteger)index
{
    if (!object)
    {
        @throw [NSException exceptionWithName:@"add object null." reason:@"object must be not null ." userInfo:nil];
        return;
    }
    ///判越界
    if ((index > _size))
    {
        @throw [NSException exceptionWithName:@"Array is out of bounds" reason:@"out of bounds" userInfo:nil];
        return;
    }
    //判断原来数组是否已经满了 如果满了就需要增加数组长度
    if (_size == _capacity)
    {
        [self resize:2 * _capacity];
    }
    //先将插入位置之后的所有数据向后移动一位
    if (self.count > 0 )
    {
        for(NSUInteger i = _size - 1 ; i >= index ; i--)
        {
            _array[i + 1] = _array[i];
        }
        
    }
    self->_array[index] = (__bridge_retained AnyObject)(object);
    _size++;
}

#pragma mark - 删除操作
- (void)removeObjectAtIndex:(NSUInteger)index
{
    ///判断越界
    if ((index > _size)) {
        @throw [NSException exceptionWithName:@"Array is out of bounds" reason:@"out of bounds" userInfo:nil];
        return;
    }
    AnyObject object =(_array[index]);
    CFRelease(object);
    for(NSInteger i = index + 1 ; i < _size ; i ++)
        _array[i - 1] = _array[i];
    _size--;
    _array[_size] = NULL;
    ///对数组空间缩减
    if (_size == _capacity * 0.25 && (_capacity*0.25 != 0))
    {
        [self resize:_capacity/2];
    }
}

- (BOOL)removeObject:(id)anObject
{
    NSInteger index = [self indexOfObject:anObject];
    if (index == NSNotFound)
    {
        return NO;
    }
    [self removeObjectAtIndex:index];
    return YES;
}

- (void)removeLastObject
{
    if ([self isEmpty])
    {
        return;
    }
    [self removeObjectAtIndex:_size-1];
}

#pragma mark - 修改操作
- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)object
{
    if (!object)
    {
        @throw [NSException exceptionWithName:@"add object null." reason:@"object must be not null ." userInfo:nil];
        return;
    }
    ///判断越界
    if ((index > _size))
    {
        @throw [NSException exceptionWithName:@"Array is out of bounds" reason:@"out of bounds" userInfo:nil];
        return;
    }
    _array[index] = (__bridge AnyObject)(object);
}

#pragma mark - 查询操作
- (NSUInteger)count
{
    return _size;
}

- (BOOL)isEmpty
{
    return self.size == 0;
}

- (BOOL)containsObject:(id)object
{
    for (int i = 0; i<_size; i++)
    {
        id obj = (__bridge id)(_array[i]);
        if ([object isEqual:obj])
        {
           return YES;
        }
    }
    return NO;
}

- (id)objectAtIndex:(NSUInteger)index
{
    if ((index > _size))
    {
        @throw [NSException exceptionWithName:@"Array is out of bounds" reason:@"out of bounds" userInfo:nil];
        return nil;
    }
    if ([self isEmpty])
    {
        return nil;
    }
    AnyObject obj = _array[index];
    if (obj == NULL)
    {
        return nil;
    }
    return (__bridge id)(obj);
}

- (NSUInteger)indexOfObject:(id)object
{
    for (int i = 0; i<_size; i++)
    {
        id obj = (__bridge id)(_array[i]);
        if ([object isEqual:obj])
        {
           return i;
        }
    }
    return NSNotFound;
}

- (id)firstObject
{
    if ([self isEmpty])
    {
        return nil;
    }
    return (__bridge id _Nullable)(_array[0]);
}

- (id)lastObject
{
    if ([self isEmpty])
    {
        return nil;
    }
    return (__bridge id _Nullable)(_array[_size-1]);
}


/**
 对数组扩容
 
 @param capacity 新的容量
 */
- (void) resize:(NSInteger)capacity
{
    _capacity = capacity;
    AnyObject *oldArray = _array;
    AnyObject *newArray = (AnyObject *)calloc(_capacity,sizeof(AnyObject));
    size_t size = sizeof(AnyObject) * self.count;
    //对旧的数组进行值的拷贝
    memcpy(newArray,oldArray,size);
    _array = newArray;
    if (oldArray != NULL)
    {
        free(oldArray);
        oldArray = NULL;
    }
}

- (NSString *)description
{
    NSMutableString *res = [NSMutableString string];
    [res appendFormat:@"ArrayList: %p \n",self];
    [res appendString:@"["];
    for (int i = 0; i<self.count; i++)
    {
        id object = [self objectAtIndex:i];
        [res appendFormat:@"%@",object];
        if (i != self.count - 1)
        {
            [res appendString:@" , "];
        }
    }
    [res appendString:@"]"];
    return res;
}

@end
