//
//  CSArray.h
//  DataStruct
//
//  Created by 余强 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CSArrayListProtocol <NSObject>

+ (instancetype)array;

+ (instancetype)arrayWithCapacity:(NSUInteger)numItems;

- (instancetype)initWithCapacity:(NSUInteger)numItems;

//增加
- (void)addObject:(id)object;

- (void)insertObject:(id)object atIndex:(NSUInteger)index;

//删除
- (void)removeObjectAtIndex:(NSUInteger)index;

- (BOOL)removeObject:(id)object;

- (void)removeLastObject;

//修改
- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)object;

//查询
- (NSUInteger)count;

- (BOOL)isEmpty;

- (BOOL)containsObject:(id)object;

- (id)objectAtIndex:(NSUInteger)index;

- (NSUInteger)indexOfObject:(id)object;

- (id)firstObject;

- (id)lastObject;

@end

@interface CSArrayList : NSObject<CSArrayListProtocol>

@end
