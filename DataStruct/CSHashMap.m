//
//  CSLinkedListMap.m
//  DataStruct
//
//  Created by 韩小猫爱吃鱼 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import "CSHashMap.h"
#import "CSArrayList.h"
#import "CSLinkedList.h"

@implementation CSMapNode

+ (instancetype)key:(NSString *)key value:(id)value
{
    CSMapNode *node = [[self alloc] init];
    node.key = key;
    node.value = value;
    return node;
}

@end

NSInteger boxCapcapacity = 999;
@interface CSHashMap()

@property (nonatomic,assign) int size;
//@property (nonatomic,strong) CSArrayList *array[boxCapcapacity];

@end

@implementation CSHashMap
{
    CSLinkedList *_array[999];
}

+ (instancetype)map
{
    CSHashMap *map = [[self alloc] init];
//    map.array = [CSArrayList array];
    map.size = 0;
    return map;
}

- (void)setObject:(id)value forKey:(NSString *)key
{
    self.size ++;

    NSUInteger hash        = key.hash;
    //默认一个对象占用8个字节
    NSUInteger realCode    = hash%(sizeof(_array)/8);
    
//    CSLinkedList *linkList = [self.array objectAtIndex:realCode];
    CSLinkedList *linkList = _array[realCode];
    if (linkList) {
        for (NSInteger index = 0; index < linkList.count; index ++) {
            
            CSMapNode *keyValue = (CSMapNode *)[linkList objectAtIndex:index];
            if ([keyValue.key isEqualToString:key]) { //如果存在相同的Key ，更新value
                keyValue.value = value; //重新赋值value
                return;
            }
        }
        CSMapNode *newKeyValue  = [CSMapNode key:key value:value];
        [linkList addObject:newKeyValue];
        
    }else{
        CSLinkedList *linkList = [CSLinkedList linklist];
        CSMapNode *newKeyValue  = [CSMapNode key:key value:value];
        [linkList addObject:newKeyValue];
//        [self.array insertObject:linkList atIndex:realCode];
        _array[realCode] = linkList;
    }
}

- (id)objectForKey:(NSString *)key
{
    if (!key.length) {
        return nil;
    }
    NSUInteger hash        = key.hash;
    NSUInteger realCode    = hash % boxCapcapacity;
//    CSLinkedList *linkList = [self.array objectAtIndex:realCode];
    CSLinkedList *linkList = _array[realCode];
    if (linkList) {
        for (NSInteger index = 0; index < linkList.count; index ++) {
            CSMapNode *keyvalue = (CSMapNode *)[linkList objectAtIndex:index];
            if ([keyvalue.key isEqualToString:key]) {
                return keyvalue.value;
            }
        }
    }
    return nil;
}

@end
