//
//  main.m
//  DataStruct
//
//  Created by 余强 on 2018/8/20.
//  Copyright © 2018年 余强. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSLinkedList.h"
#import "CSArrayList.h"
#import "CSLinkedListStack.h"
#import "CSLinkedListQueue.h"
#import "CSHashMap.h"
#import "CSBinarySearchTree.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        
        NSNumber *p1 = @5;
        NSArray *array = @[@(10),@(12),p1,@22];
        CSLinkedList *linkArray = [CSLinkedList linklist];
        for (NSInteger i = 0 ; i < array.count; i++)
        {
            [linkArray addObject:array[i]];
        }
//        [linkArray insertObject:@44 atIndex:1];
//
//        [linkArray removeObject:p1];
//
     //  [linkArray containInObject:p1];
        
   
        CSHashMap *map = [CSHashMap map];
        [map setObject:@"11" forKey:@"JACK"];
        id value = [map objectForKey:@"JACK"];
        
        NSLog(@"%@",linkArray);
        NSLog(@"************");
        
        CSArrayList *arrayList = [CSArrayList array];
        for (NSInteger i = 0 ; i < array.count; i++)
        {
            [arrayList addObject:array[i]];
        }
        NSLog(@"%@",arrayList);
        NSLog(@"************");
        
        //linkStack
        CSLinkedListStack *linkStackArray = [CSLinkedListStack linkStack];
        for (NSInteger i = 0 ; i < array.count; i++)
        {
            [linkStackArray push:array[i]];
        }
        NSLog(@"%@",linkStackArray);
        id object =  [linkStackArray pop];
        NSLog(@"%@",linkStackArray);
        object =  [linkStackArray pop];
        NSLog(@"%@",linkStackArray);
        
        NSLog(@"************");
        //linkQueue
        CSLinkedListQueue *linkQueueArray = [CSLinkedListQueue linkQueue];
        for (NSInteger i = 0 ; i < array.count; i++)
        {
            [linkQueueArray queue:array[i]];
        }
        NSLog(@"%@",linkQueueArray);
        object =  [linkQueueArray dequeue];
        NSLog(@"%@",linkQueueArray);
        object =  [linkQueueArray dequeue];
        NSLog(@"%@",linkQueueArray);
        
        
        CSBinarySearchTree *bst = [CSBinarySearchTree bst];
        for (NSInteger i = 0 ; i < array.count; i++)
        {
            [bst addObject:array[i]];
        }
    }
    return 0;
}
