//
//  CSLinkedListMap.h
//  DataStruct
//
//  Created by 韩小猫爱吃鱼 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import "CSLinkedList.h"
#import "CSMapProtocol.h"

@interface CSMapNode : NSObject

@property (nonatomic,copy) NSString *key;
@property (nonatomic,strong) id value;

+ (instancetype)key:(NSString *)key value:(id)value;

@end

@interface CSHashMap : NSObject<CSMapProtocol>

+ (instancetype)map;

@end
