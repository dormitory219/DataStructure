//
//  CSArrayListStack.m
//  DataStruct
//
//  Created by 余强 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import "CSArrayListStack.h"
#import "CSArrayList.h"

@interface CSArrayListStack()
{
    //使用动态数组作为底层结构
    CSArrayList *_arrayList;
}
@end

@implementation CSArrayListStack

- (instancetype)init
{
    self = [super init];
    if (self) {
        _arrayList = [CSArrayList array];
    }
    return self;
}

+ (instancetype)arrayStack
{
    return [[self alloc] init];
}

- (NSInteger)count
{
    return _arrayList.count;
}

- (void)push:(id)object
{
    [_arrayList addObject:object];
}

- (id)pop
{
    id object = [_arrayList lastObject];
    [_arrayList removeObject:object];
    return object;
}

- (NSString *)description
{
    NSMutableString *res = [NSMutableString string];
    [res appendFormat:@"ArrayListStack: %p \n",self];
    [res appendString:@" [ "];
    for (int i = 0; i<_arrayList.count; i++)
    {
        id object = [_arrayList objectAtIndex:i];
        [res appendFormat:@"%@",object];
        if (i != _arrayList.count - 1)
        {
            [res appendString:@" , "];
        }
    }
    [res appendString:@" ] top "];
    return res;
}

@end
