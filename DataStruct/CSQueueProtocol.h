//
//  CSQueueProtocol.h
//  DataStruct
//
//  Created by 韩小猫爱吃鱼 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#ifndef CSQueueProtocol_h
#define CSQueueProtocol_h

@protocol CSQueueProtocol <NSObject>

- (NSInteger)count;

- (void)queue:(id)object;

- (id)dequeue;

@end

#endif /* CSQueueProtocol_h */
