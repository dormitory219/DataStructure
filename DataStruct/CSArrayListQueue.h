//
//  CSArrayListQueue.h
//  DataStruct
//
//  Created by 余强 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSQueueProtocol.h"

@interface CSArrayListQueue : NSObject<CSQueueProtocol>

+ (instancetype)arrayQueue;

@end
