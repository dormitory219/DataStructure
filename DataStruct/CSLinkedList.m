//
//  CSLinkedList.m
//  DataStruct
//
//  Created by 余强 on 2018/8/20.
//  Copyright © 2018年 余强. All rights reserved.
//

#import "CSLinkedList.h"

@implementation CSNode

+ (instancetype)node
{
    return [self nodeWithData:nil nextNode:nil];
}

+ (instancetype)nodeWithData:(id)data
{
     return [self nodeWithData:data nextNode:nil];
}

+ (instancetype)nodeWithData:(id)data nextNode:(CSNode *)nextNode
{
    CSNode *node = [[self alloc] init];
    node.data = data;
    node.next = nextNode;
    return node;
}

@end

@interface CSLinkedList()

@property (nonatomic,assign) int size;
//只持有dummyHead头节点,也可持有尾节点
@property (nonatomic,strong) CSNode *dummyHead;

@end

@implementation CSLinkedList

- (instancetype)init
{
    self = [super init];
    if (self) {
        //设置虚拟头节点，但不占index
        CSNode *dummyHead = [CSNode node];
        self.dummyHead = dummyHead;
        self.size = 0;
    }
    return self;
}

+ (instancetype)linklist
{
    return [[self alloc] init];
}

- (int)count
{
    return self.size;
}

- (BOOL)isEmpty
{
    return self.size == 0;
}

- (BOOL)containsObject:(id)object
{
    if (!object)
    {
        NSLog(@"object cannot be nil");
        return NO;
    }
    
    CSNode *node = self.dummyHead;
    while (node)
    {
        if ([node.data isEqual:object])
        {
            return YES;
        }
        else
        {
            node = node.next;
        }
    }
    return YES;
}

- (id)firstObject
{
    CSNode *node = self.dummyHead;
    return node.next.data;
}

- (id)lastObject
{
    CSNode *node = self.dummyHead;
    while (node.next)
    {
        node = node.next;
    }
    return node.data;
}

- (id)objectAtIndex:(NSUInteger)index
{
    CSNode *node = self.dummyHead;
    NSInteger enumIndex = 0;
    while (node)
    {
        if (node.next)
        {
            if (enumIndex == index)
            {
                return node.data;
            }
            else
            {
                node = node.next;
                enumIndex ++;
            }
        }
        else
        {
            return nil;
        }
    }
    return nil;
}

- (void)addObject:(id)object
{
   CSNode *node = self.dummyHead;
    while (node)
    {
        if (!node.next)
        {
            node.next = [CSNode nodeWithData:object nextNode:nil];
            self.size++;
            return;
        }
        else
        {
            node = node.next;
        }
    }
}

- (void)insertObject:(id)object atIndex:(int)index
{
    CSNode *node = self.dummyHead;
    if (!object)
    {
        @throw [NSException exceptionWithName:@"add object null." reason:@"object must be not null ." userInfo:nil];
        return;
    }
    int enumIndex = 0;
    while (node)
    {
        if (enumIndex == index)
        {
//            CSNode *newNode = [CSNode nodeWithValue:object];
//            newNode.next = node.next;
//            node.next = newNode;
            node.next = [CSNode nodeWithData:object nextNode:node.next];
            self.size ++;
            return;
        }
        else
        {
            node = node.next;
            enumIndex ++;
        }
    }
}

- (BOOL)removeObject:(id)object
{
    if ([self containsObject:object])
    {
        CSNode *node = self.dummyHead;
        CSNode *lastNode = node;
        while (node)
        {
            if ([node.data isEqual:object])
            {
                lastNode.next = node.next;
                node.next = nil;
                node = nil;
                self.size --;
            }
            else
            {
                lastNode = node;
                node = node.next;
            }
        }
    }
    else
    {
        return NO;
    }
    return YES;
}

- (NSString *)description
{
    NSMutableString *string = [NSMutableString stringWithFormat:@"\nLinkedList %p : [ " ,self];
    [string appendString:@"NULL -> "];
    CSNode *cur = self.dummyHead.next;
    while (cur != nil)
    {
        [string appendFormat:@"%@",cur.data];
        cur = cur.next;
        if (cur)
        {
            [string appendFormat:@" -> "];
        }
    }
    [string appendString:@" ]\n"];
    return string;
}

@end
