//
//  CSBinarySearchTree.m
//  DataStruct
//
//  Created by 余强 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import "CSBinarySearchTree.h"

@implementation CSBSTNode

+ (instancetype)node
{
    return [self nodeWithData:nil];
}

+ (instancetype)nodeWithData:(id)data
{
    CSBSTNode *node = [[self alloc] init];
    node.data = data;
    node.left = nil;
    node.right = nil;
    return node;
}

- (BOOL)compareToData:(id)data
{
    NSComparisonResult result = [self.data compare:data];
    switch (result) {
        case NSOrderedAscending:
             //小于
            return NO;
            break;
        case NSOrderedSame:
            return YES;
            break;
        case NSOrderedDescending:
             //大于
            return YES;
            break;
        default:
            break;
    }
    return YES;
}

@end

@interface CSBinarySearchTree()

@property (nonatomic,assign) NSInteger size;
@property (nonatomic,strong) CSBSTNode *root;

@end

@implementation CSBinarySearchTree

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.root = nil;
        self.size = 0;
    }
    return self;
}

+ (instancetype)bst
{
    return [[self alloc] init];
}

- (void)addObject:(id)object
{
    [self addNode:self.root data:object];
}

- (void)addNode:(CSBSTNode *)node data:(id)data
{
    if (!node)
    {
        self.root = [CSBSTNode nodeWithData:data];
    }
    else
    {
        if ([node compareToData:data] && !node.left)
        {
            node.left = [CSBSTNode nodeWithData:data];
        }
        else if (![node compareToData:data] && !node.right)
        {
            node.right = [CSBSTNode nodeWithData:data];
        }
        else
        {
            if ([node compareToData:data])
            {
                [self addNode:node.left data:data];
            }
            else if (![node compareToData:data])
            {
                [self addNode:node.right data:data];
            }
        }
    }
}

- (NSString *)description
{
    return nil;
}

@end
