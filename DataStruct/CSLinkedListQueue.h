//
//  CSLinkedListQueue.h
//  DataStruct
//
//  Created by 韩小猫爱吃鱼 on 2018/8/21.
//  Copyright © 2018年 余强. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSQueueProtocol.h"

@interface CSLinkedListQueue : NSObject<CSQueueProtocol>

+ (instancetype)linkQueue;

@end
